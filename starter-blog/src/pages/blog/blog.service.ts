import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { Post } from 'types/blog.type'

/*
  Nếu bên slice chúng ta dùng createSlice để tạo slice thì bên RTK query dùng createApi
   + với createApi chúng ta gọi là slice api
   + nơi khai báo các baseUrl và các endpoits
*/

/*
  baseQuery: dùng cho mỗi endpoint để fetch api
  fetchBaseQuery là 1 function nhỏ được xây dựng trên fetch API
    + không hoàn toàn thay thế được axios nhưng sẽ giải quyết được hầu hết bài toán cơ bản
    + có thể dùng axios thay thế cũng được
*/

/*
  enPoints: là tập hợp những method giúp GET, POST, PUT, DELETE, ...
  khi khai báo enpoints nó sẽ sinh ra các HOOK TƯƠNG ỨNG để dùng trong component
    + có 2 kiểu là QUERY và MUTATION
      - query: thường dùng cho get
      - mutation: thường dùng cho các trường hợp update data như POST, PUT, DELETE
*/

export const blogApi = createApi({
  reducerPath: 'blogApi', // tên field trong Redux state
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:4000/' }),
  endpoints: (build) => ({
    // Generic tyoe theo thứ tự là kiểu response trả về và argument (để void nếu không truyền gì)
    getPosts: build.query<Post[], void>({
      query: () => 'posts' // http://localhost:4000/posts (method không có argument)
    })
  })
})

export const { useGetPostsQuery } = blogApi // hook useGetPostsQuery sẽ tự động được sinh ra
